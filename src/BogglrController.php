<?php

include 'BogglrFind.php';
include 'BogglrBoard.php';

/**
 * Class BogglrController
 *
 * Provides a command-line interface for running a Bogglr search.
 *
 * @author John Berube <john@jberube.com>
 */
class BogglrController {

    /**
     * The path to the default board file.
     */
    const DEFAULT_BOARD_FILE_NAME = 'boards/sample/sample1.txt';

    /**
     * Output text to CLI
     *
     * Outputs a string and adds a carriage return at the end.
     *
     * @param string $str
     * @return void
     */
    private static function output($str) {
        print "$str\n";
    }

    /**
     * Run the CLI program
     *
     * Prompt user for a board file, then for a word to search, and display the result.
     *
     * @return void
     */
    public static function run()
    {

        // Prompt user for a text file containing the Bogglr board
        $boardFileName = readline('Enter board file name relative to bogglr directory [hit enter for ' . self::DEFAULT_BOARD_FILE_NAME . ']: ');

        // If user hit enter, use default
        if (empty($boardFileName)) {
            $boardFileName = self::DEFAULT_BOARD_FILE_NAME;
        }

        // Create a BogglrBoard object from the file
        try {
            $board = new BogglrBoard($boardFileName);
        } catch (Exception $e) {
            self::output($e->getMessage());
            exit;
        }

        // Display the board
        self::output("Your board: \n" . $board->getBoardGridDisplay());

        // Prompt user for word to find on the Bogglr board
        while (!$searchWord = trim(strtoupper(readline('Enter the word you would you like to find (case-insensitive): ')))) {
            self::output('Please enter a word.');
        }
        self::output('Searching for: ' . $searchWord);

        // Find the word on the Bogglr board
        $bogglrFind = new BogglrFind($board);
        $isFound = $bogglrFind->findWord($searchWord);

        // Output result
        self::output('Result: ' . ($isFound ? 'TRUE' : 'FALSE'));
    }

}
