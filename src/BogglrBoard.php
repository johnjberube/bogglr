<?php

/**
 * Class BogglrBoard
 *
 * Represents a two-dimensional Board of lettered tiles
 *
 * @author John Berube <john@jberube.com>
 */
class BogglrBoard {

    /**
     * Exception code for a file not found error.
     */
    const EXCEPTION_CODE_FILE_NOT_EXIST = 1;

    /**
     * Exception code for an error opening a file.
     */
    const EXCEPTION_CODE_CANT_OPEN = 2;

    /**
     * Exception code for an empty board.
     */
    const EXCEPTION_CODE_EMPTY_BOARD = 3;

    /**
     * A string representing all the letters of the board. Rows are separated by carriage returns.
     *
     * @var string
     */
    private $boardStr = '';

    /**
     * A 2-D array of letters located at x,y coordinates.
     *
     * @var array
     */
    private $boardGrid = [];

    /**
     * Constructor
     *
     * Initialize board.
     *
     * @param string $boardFileName The file path to a text file containing letters, relative to the top level execution
     *   script.
     * @return void
     * @throws Exception
     */
    public function __construct(string $boardFileName) {
        $this->boardStr = $this->loadFromFile($boardFileName);
        $this->boardGrid = $this->buildBoardFromStr($this->boardStr);
    }

    /**
     * Load from file
     *
     * Get a string of board tiles, loaded from a text file at the given a file path.
     *
     * @param string $boardFileName The file path to a text file containing letters, relative to the top level execution
     *   script.
     * @return string
     * @throws Exception
     */
    private function loadFromFile(string $boardFileName) {
        if (!file_exists($boardFileName)) {
            throw new Exception("Couldn't find board file: $boardFileName", self::EXCEPTION_CODE_FILE_NOT_EXIST);
        }

        try{
            $board = file_get_contents($boardFileName);
        } catch (Exception $e) {
            throw new Exception('Error opening board file. Message: ' . $e->getMessage(), self::EXCEPTION_CODE_CANT_OPEN);
        }

        if (empty($board)) {
            throw new Exception('Board file is empty.', self::EXCEPTION_CODE_EMPTY_BOARD);
        }

        return strtoupper($board);
    }

    /**
     * Build board from string
     *
     * Given a string of letters, return a 2-d array of letters, keyed by x and y coordinates.
     *
     * @param string $boardStr A string representing all the letters of the board. Rows are separated by carriage
     *   returns.
     * @return array
     */
    private function buildBoardFromStr(string $boardStr) {
        $boardGrid = [];
        $boardRows = explode("\n", $boardStr);

        foreach ($boardRows as $y => $boardRow) {
            $cols = str_split($boardRow);

            foreach ($cols as $x => $col) {
                $boardGrid[$x][$y] = $col;
            }
        }

        return $boardGrid;
    }

    /**
     * Get board grid
     *
     * Get the 2-d array representing the board tiles.
     *
     * @return array
     */
    public function getBoardGrid() {
        return $this->boardGrid;
    }

    /**
     * Get letter
     *
     * Get the letter at the given coordinates, or null if none exists at those coordinates.
     *
     * @param int $x
     * @param int $y
     * @return string|null
     */
    public function getLetter(int $x, int $y) {
        return isset($this->boardGrid[$x][$y]) ? $this->boardGrid[$x][$y] : null;
    }

    /**
     * Get board grid display
     *
     * Get a string representing the board tiles visually.
     *
     * @return string
     */
    public function getBoardGridDisplay() {

        $letters = str_split($this->boardStr);
        $str = '';

        foreach ($letters as $letter) {
            $str .= ' ' . $letter;
        }

        return $str;
    }

}