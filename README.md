# Bogglr
A word search validator program, written in PHP. 

Submitted to Tumblr by John Berube on May 21, 2019

## Requirements
PHP version 7.1 or higher.

## Setup
You optionally may create your own Bogglr board by creating a text file and placing it in the "boards/user" directory. In the text file, enter all your letters from left to right, and separate rows with carriage returns. 

Example: 
```
TEST
SCOD
NXPA
UNIT
```


## Execution
At the command prompt, change to this directory and type:
```
php bogglr.php
```

You will be prompted to enter the file path to your Bogglr board. Type the file path relative to this directory and hit enter. You optionally may hit enter without typing a path to use the default sample board.

Your board will then be displayed. If not, an error message should appear.

Next, you will be prompted for the word to find. Type the word and hit enter. Your word will be trimmed for spaces and converted to uppercase. If you don't enter any non-whitespace characters, you will be prompted again until you do.

Finally, you will see a message displaying the word being searched, and the result, which will be TRUE if the word is found, and FALSE if it is not.
 
 
 
## Unit Testing
Some unit tests were created to ensure code integrity. To perform unit testing, you must first run composer install from this directory. This will install PHPUnit.
```
composer install
```

To run the tests, use the command:
```
vendor/bin/phpunit --bootstrap vendor/autoload.php tests/BogglrTest
```
