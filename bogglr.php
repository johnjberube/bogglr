<?php

/**
 * @file
 * Bogglr bootstrap file.
 *
 * Checks for required PHP version and interface, outputs a title, then runs program.
 *
 * @author John Berube <john@jberube.com>
 */

// Ensure this is being run from a command line interface, and not a web browser
if (php_sapi_name() != 'cli') {
    print "This program must be run from a command-line interface, not a web browser.\n";
    exit;
}

// Ensure that running PHP version is > 7.1
if (version_compare(phpversion(), '7.1', '<')) {
    print "This program requires PHP version 7.1 or higher.\n";
    exit;
}

print <<<EOT
======================================================

#######\                                ##\
##  __##\                               ## |
## |  ## | ######\   ######\   ######\  ## | ######\
#######\ |##  __##\ ##  __##\ ##  __##\ ## |##  __##\
##  __##\ ## /  ## |## /  ## |## /  ## |## |## |  \__|
## |  ## |## |  ## |## |  ## |## |  ## |## |## |      
#######  |\######  |\####### |\####### |## |## |
\_______/  \______/  \____## | \____## |\__|\__|
                    ##\   ## |##\   ## |
                    \######  |\######  |
                     \______/  \______/               
                     
======================================================
Find out if a word can be found on a Bogglr board. 
See README.md for more instructions.
======================================================

EOT;

include 'src/BogglrController.php';

BogglrController::run();