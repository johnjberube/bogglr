<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

require __DIR__ . "/../src/BogglrBoard.php";
require __DIR__ . "/../src/BogglrFind.php";

/**
 * Class BogglrTest
 *
 * A PHPUnit test class for the various Bogglr classes
 *
 * @author John Berube <john@jberube.com>
 */
final class BogglrTest extends TestCase
{

    /**
     * The path to the default board file.
     */
    const TEST_BOARD_FILE_NAME = 'boards/sample/sample1.txt';

    /**
     * Assert that a BogglrBoard can be created from a valid board text file
     *
     * @return void
     */
    public function testCanBeCreatedFromBoardFile(): void {
        $bogglrBoard = new BogglrBoard(self::TEST_BOARD_FILE_NAME);

        $this->assertInstanceOf(
            BogglrFind::class,
            new BogglrFind($bogglrBoard)
        );
    }

    /**
     * Assert that a BogglrBoard cannot be created from an invalid board text file
     *
     * @return void
     */
    public function testCannotBeCreatedFromMissingBoardFile(): void {
        try{
            new BogglrBoard('bogus_file_name');
            $this->fail("Expected exception not thrown.");
        } catch (Exception $e) { //Not catching a generic Exception or the fail function is also catched
            $this->assertEquals(BogglrBoard::EXCEPTION_CODE_FILE_NOT_EXIST, $e->getCode());
        }
    }

    /**
     * Assert that the validation program returns expected results, given a sample board and sample inputs.
     *
     * @return void
     */
    public function testReturnsExpectedResultsFromWords(): void {
        $bogglrBoard = new BogglrBoard(self::TEST_BOARD_FILE_NAME);
        $bogglrFind = new BogglrFind($bogglrBoard);

        $this->assertTrue($bogglrFind->findWord('TEST'));
        $this->assertFalse($bogglrFind->findWord('CODE'));
        $this->assertTrue($bogglrFind->findWord('POST'));
        $this->assertFalse($bogglrFind->findWord('ADS'));
        $this->assertTrue($bogglrFind->findWord('API'));
        $this->assertFalse($bogglrFind->findWord('DATA'));
        $this->assertTrue($bogglrFind->findWord('DOCS'));
        $this->assertFalse($bogglrFind->findWord('UNIX'));
        $this->assertTrue($bogglrFind->findWord('UNIT'));
    }

}