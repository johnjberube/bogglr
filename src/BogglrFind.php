<?php

/**
 * Class BogglrFind
 *
 * Performs word searches on a given Bogglr board.
 *
 * @author John Berube <john@jberube.com>
 */
class BogglrFind {

    /**
     * Bogglr board to search.
     *
     * @var BogglrBoard
     */
    private $board;

    /**
     * Array of letters in word to find.
     *
     * @var array
     */
    private $searchLetters = [];

    /**
     * Array of tiles already used in the current word path, represented by "X,Y" coordinate string.
     *
     * @var array
     */
    private $usedTiles = [];

    /**
     * Constructor
     *
     * Initializes global variables.
     *
     * @param BogglrBoard $board An object representing the Bogglr board to be searched.
     * @return void
     */
    public function __construct(BogglrBoard $board) {
        $this->board = $board;
    }

    /**
     * Find a word
     *
     * Returns true or false whether the supplied word can be found on the Bogglr board, with all the letters touching
     *   orthogonally, and not using the same tile twice in one word.
     *
     * @param string $searchWord Word to search
     * @return bool
     */
    public function findWord(string $searchWord) {
        $this->searchLetters = str_split($searchWord);
        $this->usedTiles = [];
        $cols = $this->board->getBoardGrid();

        foreach ($cols as $x => $col) {
            foreach ($col as $y => $letter) {
                if ($this->findWordRecursively($x, $y)) {
                    return TRUE;
                }
            }
        }

        return FALSE;
    }

    /**
     * Set Used Tile
     *
     * Indicate that the tile at coordinate x,y has been used in the current search path.
     *
     * @param int $x X coordinate of the tile
     * @param int $y Y coordinate of the tile
     * @return void
     */
    private function setUsedTile(int $x, int $y) {
        $this->usedTiles[] = "$x,$y";
    }

    /**
     * Unset last used tile
     *
     * Remove the last tile from the used tiles stack.
     *
     * @return void
     */
    private function unsetLastUsedTile() {
        array_pop($this->usedTiles);
    }

    /**
     * Is used tile
     *
     * Returns true or false whether the tile at the supplied coordinates has been used in the current word path.
     *
     * @param int $x X coordinate of the tile
     * @param int $y Y coordinate of the tile
     * @return bool
     */
    private function isUsedTile(int $x, int $y) {
        return in_array("$x,$y", $this->usedTiles);
    }

    /**
     * Find word recursively
     *
     * Given a coordinate for a tile on the Bogglr board, check if the tile's letter matches the current letter in the
     * searchLetters array. If matched, move the internal pointer in the searchLetters array to the next letter and then
     * call this function recursively for each adjacent tile. If the next letter isn't found in any direction, move the
     * internal pointer in the searchLetters array back to the previous position. If all the letters are found, then the
     * function will return TRUE, otherwise it will return FALSE.
     *
     * @param int $x X coordinate of the current tile
     * @param int $y Y coordinate of the current tile
     * @return bool
     */
    private function findWordRecursively(int $x, int $y) {
        // If tile at x,y has already been used in the current search path, return false
        if ($this->isUsedTile($x, $y)) {
            return FALSE;
        }

        // The letter of the tile at x,y on the Bogglr board
        $tileLetter = $this->board->getLetter($x, $y);

        // The current letter in the search path
        $searchLetter = current($this->searchLetters);

        // If the current letter of the search word is not a match for the letter on the current tile
        if ($searchLetter != $tileLetter) {
            return FALSE;
        }

        // Indicate that the current tile has been used so it won't be used again on this search path.
        $this->setUsedTile($x, $y);

        // Move the internal pointer in the searchLetters array to the next letter
        $nextLetter = next($this->searchLetters);

        // If there is no next letter, it means all the letters have been found!
        if ($nextLetter === FALSE) {
            return TRUE;
        }

        // Go right one tile and find the remaining letters
        if ($this->findWordRecursively($x + 1, $y)) {
            return TRUE;
        }

        // Go down one tile and find the remaining letters
        if ($this->findWordRecursively($x, $y + 1)) {
            return TRUE;
        }

        // Go left one tile and find the remaining letters
        if ($this->findWordRecursively($x - 1, $y)) {
            return TRUE;
        }

        // Go up one tile and find the remaining letters
        if ($this->findWordRecursively($x, $y - 1)) {
            return TRUE;
        }

        // Free up this tile to be used again in a search path
        $this->unsetLastUsedTile();

        // Move the searchLetters internal pointer back to the previous letter
        prev($this->searchLetters);

        return FALSE;
    }

}